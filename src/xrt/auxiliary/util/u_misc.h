// Copyright 2019, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Very small misc utils.
 * @author Jakob Bornecrantz <jakob@collabora.com>
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif



#ifdef __cplusplus
}
#endif
